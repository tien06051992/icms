console.log("index");

var main = function() {
    this.relatedArticleSlider = function(caruosel) {
        $(caruosel).owlCarousel({
            items: 4,
            loop: false,
            margin: 30,
            nav: true,
            navText: ['<span class="glyphicon glyphicon-menu-left"></span>','<span class="glyphicon glyphicon-menu-right"></span>'],
            callbacks: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:3,
                    nav:false
                },
                1000:{
                    items:4,
                    nav:true,
                    loop:false
                }
            }
        });
    };
}
