module.exports = {
    paths: {
        // Dependencies and current project directories to watch
        watched: [
            "app",
            "html",

            /*"bower_components/jquery/dist/jquery.min.js",
            "bower_components/bootstrap/dist/js/bootstrap.min.js",
            "bower_components/owl.carousel/dist/owl.carousel.min.js",

            "bower_components/bootstrap/dist/css/bootstrap.min.css",*/
            // "bower_components/font-awesome/css/font-awesome.min.css",
            // "bower_components/owl.carousel/dist/owl.carousel.min.css",
            // "bower_components/magnific-popup/dist/magnific-popup.css",
        ],

        // Where to compile files to
        public: "dist"
    },

    files: {
        javascripts: {
            joinTo: {
                // 'vendor.js': /^(?!app)/,
                // 'app.js': /^app/,
                /*"js/vendor.js": [
                    // "node_modules/!**",

                    "bower_components/jquery/dist/jquery.min.js",
                    "bower_components/bootstrap/dist/js/bootstrap.min.js",
                    "bower_components/owl.carousel/dist/owl.carousel.min.js",
                ],*/

                "js/index.js": "app/js/index.js",
            }
        },
        stylesheets: {
            // defaultExtension: 'scss',
            // joinTo: 'app.css',
            joinTo: {
                /*"css/app.css": [
                    "bower_components/bootstrap/dist/css/bootstrap.min.css",
                    // "bower_components/font-awesome/css/font-awesome.min.css",
                    // "bower_components/owl.carousel/dist/owl.carousel.min.css",
                    // "bower_components/magnific-popup/dist/magnific-popup.css",

                    "app/scss/theme.scss",
                ],*/

                "css/theme.css": "app/scss/theme.scss",
                "css/index.css": "app/scss/index.scss",
                "css/related-article-slider.css": "app/scss/related-article-slider.scss",
                "css/master.css": "app/scss/master.scss",
                "css/blog-article-box.css": "app/scss/blog-article-box.scss",
            },
        }
    },

    plugins: {
        // babel: {
        //     only: /\.jsx$/,
        //     presets: ['es2015', 'react'],
        //     // ignore: [/bower_components\/moment/]
        // },
        postcss: {
            processors: [
                require('autoprefixer')
            ]
        }
    },

    modules: {
        wrapper: false,
        // autoRequire: {
        //     "app.js": ["components/App"]
        // }
    },

    sourceMaps: false
};
