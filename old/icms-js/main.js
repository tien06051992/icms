$(document).ready(function() {

	/**
	 * Sticky slider top product
	 */
	var topProduct = $(".top-product");
	var anchorYStickyTopProduct = topProduct.offset().top ;
	console.log(anchorYStickyTopProduct);
    $(window).scroll(function() {
		if($(window).scrollTop() > anchorYStickyTopProduct) {
	       if(!topProduct.hasClass("sticky-slider")) {
	       		topProduct.addClass("sticky-slider");
	       		console.log(1);
	       }
	   	}else {
	   		if(topProduct.hasClass("sticky-slider")) {
	       		topProduct.removeClass("sticky-slider");
	       		console.log(2);
	       }
	   	}
	});

	/* caruosel related post */
	$("#top-product").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds
	    items : 9,
	    itemsDesktop : [1199,9],
	    itemsDesktopSmall : [979,3],
	    itemsMobile: [479,1]
	});


	// random value for rating
	function randomRating() {
		return Math.floor((Math.random() * 100) + 1);
	}
    $('.wrap-rating-item .rating-item').click(function(){
    	rat1 = randomRating();
    	rat2 = randomRating();
    	rat3 = randomRating();
    	rat4 = randomRating();
    	$('#rating-bar1 span').css('width', rat1 + '%');
    	$('#rating-bar2 span').css('width', rat2 + '%');
    	$('#rating-bar3 span').css('width', rat3 + '%');
    	$('#rating-bar4 span').css('width', rat4 + '%');
    	console.log(rat1);
    	$('.wrap-rating-item .rating-item').removeClass('active');
    	$(this).addClass('active');
    });
});