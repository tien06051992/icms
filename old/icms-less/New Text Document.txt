/*
    Created on : May 2, 2015,
    Author     : Tien Le
*/
@import "variables.less";

/* #Tablet (Landscape) 1024px
---------------------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 960px) and (max-width: 1240px) {
	.gold-product {
		width: 15%;
		label {
			font-size: 7px;
		}
	}

	 .product-slider.home-slider {
	 	width: 84%;
	}

}
/* #Tablet (Portrait) 768px
---------------------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 768px) and (max-width: 959px) {
	.gold-product {
		width: 15%;
		label {
			font-size: 7px;
		}
	}

	 .product-slider.home-slider {
	 	width: 84%;
	}

}
/* #Mobile (Landscape) 480px
---------------------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 480px) and (max-width: 767px) {
	.gold-product {
		width: 50%;
		label {
			font-size: 7px;
		}
	}

	 .product-slider.home-slider {
	 	width: 49%;
	}
}


/*  #Mobile (Portrait) 320px
---------------------------------------------------------------------------------------------------------------*/
@media only screen and (min-width: 100px) and (max-width: 479px) {
	.gold-product {
		width: 50%;
		label {
			font-size: 7px;
		}
	}

	 .product-slider.home-slider {
	 	width: 49%;
	}
}

